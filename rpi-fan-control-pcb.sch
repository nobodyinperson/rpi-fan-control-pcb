EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_FET:IRLML6402 Q1
U 1 1 5E4840E7
P 5300 2400
F 0 "Q1" H 5506 2354 50  0000 L CNN
F 1 "IRLML6402" H 5506 2445 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 5500 2325 50  0001 L CIN
F 3 "https://www.infineon.com/dgdl/irlml6402pbf.pdf?fileId=5546d462533600a401535668d5c2263c" H 5300 2400 50  0001 L CNN
	1    5300 2400
	-1   0    0    1   
$EndComp
$Comp
L Connector:Raspberry_Pi_2_3 J1
U 1 1 5E486433
P 6900 3000
F 0 "J1" H 6900 4481 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 6900 4390 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 6900 3000 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 6900 3000 50  0001 C CNN
	1    6900 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2400 5500 2400
$Comp
L Motor:Fan M1
U 1 1 5E48BDA6
P 5500 1800
F 0 "M1" H 5658 1896 50  0000 L CNN
F 1 "Fan" H 5658 1805 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 5500 1810 50  0001 C CNN
F 3 "~" H 5500 1810 50  0001 C CNN
	1    5500 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2000 5200 2000
Wire Wire Line
	5200 2000 5200 2200
Wire Wire Line
	5200 2600 5200 4450
Wire Wire Line
	5200 4450 6500 4450
Wire Wire Line
	6500 4450 6500 4300
Wire Wire Line
	5500 1500 5500 1350
Wire Wire Line
	5500 1350 6700 1350
Wire Wire Line
	6700 1350 6700 1550
Wire Wire Line
	6700 1550 6800 1550
Wire Wire Line
	6800 1550 6800 1700
Connection ~ 6700 1550
Wire Wire Line
	6700 1550 6700 1700
$Comp
L Device:D D1
U 1 1 5E4957C5
P 5150 1750
F 0 "D1" V 5196 1829 50  0000 L CNN
F 1 "D" V 5105 1829 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P7.62mm_Horizontal" H 5150 1750 50  0001 C CNN
F 3 "~" H 5150 1750 50  0001 C CNN
	1    5150 1750
	0    -1   1    0   
$EndComp
Wire Wire Line
	5150 1600 5150 1350
Wire Wire Line
	5150 1350 5500 1350
Connection ~ 5500 1350
Wire Wire Line
	5150 1900 5150 2000
Wire Wire Line
	5150 2000 5200 2000
Connection ~ 5200 2000
NoConn ~ 6100 2100
NoConn ~ 6100 2200
NoConn ~ 6100 2400
NoConn ~ 6100 2800
NoConn ~ 6100 2900
NoConn ~ 6100 3000
NoConn ~ 6100 3200
NoConn ~ 6100 3300
NoConn ~ 6100 3400
NoConn ~ 6100 3500
NoConn ~ 6100 3600
NoConn ~ 6100 3700
NoConn ~ 7700 3800
NoConn ~ 7700 3700
NoConn ~ 7700 3500
NoConn ~ 7700 3400
NoConn ~ 7700 3300
NoConn ~ 7700 3200
NoConn ~ 7700 3100
NoConn ~ 7700 2900
NoConn ~ 7700 2800
NoConn ~ 7700 2700
NoConn ~ 7700 2500
NoConn ~ 7700 2400
NoConn ~ 7700 2200
NoConn ~ 7700 2100
NoConn ~ 7000 1700
NoConn ~ 7100 1700
$Comp
L power:+5V #PWR0101
U 1 1 5E487F6A
P 6700 1050
F 0 "#PWR0101" H 6700 900 50  0001 C CNN
F 1 "+5V" H 6715 1223 50  0000 C CNN
F 2 "" H 6700 1050 50  0001 C CNN
F 3 "" H 6700 1050 50  0001 C CNN
	1    6700 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1050 6700 1350
Connection ~ 6700 1350
$Comp
L power:GND #PWR0102
U 1 1 5E489530
P 6500 4450
F 0 "#PWR0102" H 6500 4200 50  0001 C CNN
F 1 "GND" H 6505 4277 50  0000 C CNN
F 2 "" H 6500 4450 50  0001 C CNN
F 3 "" H 6500 4450 50  0001 C CNN
	1    6500 4450
	1    0    0    -1  
$EndComp
Connection ~ 6500 4450
Wire Wire Line
	6500 4300 6600 4300
Connection ~ 6500 4300
Connection ~ 6600 4300
Wire Wire Line
	6600 4300 6700 4300
Connection ~ 6700 4300
Wire Wire Line
	6700 4300 6800 4300
Connection ~ 6800 4300
Wire Wire Line
	6800 4300 6900 4300
Connection ~ 6900 4300
Wire Wire Line
	6900 4300 7000 4300
Connection ~ 7000 4300
Wire Wire Line
	7000 4300 7100 4300
Connection ~ 7100 4300
Wire Wire Line
	7100 4300 7200 4300
Wire Wire Line
	5750 2600 6100 2600
Wire Wire Line
	5750 2400 5750 2600
NoConn ~ 6100 2500
$EndSCHEMATC
