#!/usr/bin/env python3
# system modules
import time
import logging
import sys

# internal modules

# external modules
from simple_pid import PID
import RPi.GPIO as IO

IO.setmode(IO.BCM)

logger = logging.getLogger(__name__)

class FanControl(PID):
    def __init__(self, *args, **kwargs):
        pid_kwargs = {"output_limits":(0,100), "setpoint":40,"sample_time":5}
        pid_kwargs.update(kwargs)
        PID.__init__(self,*args, **pid_kwargs)
        self.tunings = (1,1,1)

    @property
    def pwm(self):
        try:
            return self._pwm
        except AttributeError:
            IO.setup(18, IO.OUT, initial=IO.HIGH)
            self._pwm = IO.PWM(18, 25)
            self._pwm.start(50)
        return self._pwm

    @property
    def temperature(self):
        with open("/sys/class/thermal/thermal_zone0/temp") as fh:
            return float(fh.read()) / 1000

    @property
    def fan_speed(self):
        return getattr(self,"_fan_speed",None)

    @fan_speed.setter
    def fan_speed(self, speed):
        speed = min(100, max(0,float(speed)))
        if speed != self.fan_speed:
            self.pwm.ChangeDutyCycle(speed)
            self._fan_speed = speed

    def regulate(self):
        self.fan_speed = self(self.temperature)

logging.basicConfig(level=logging.DEBUG)

control = FanControl()

try:
    last_speed = control.fan_speed or 0
    last_temperature = control.temperature
    while True:
        control.regulate()
        speed, temperature = control.fan_speed, control.temperature
        speed_change = abs(speed - last_speed)
        temperature_change = abs(temperature - last_temperature)
        if speed_change > 10 or temperature_change > 5:
            print("{:.1f},{}".format(speed,temperature))
            sys.stdout.flush()
            last_speed = speed
            last_temperature = temperature
        time.sleep(0.1)
except KeyboardInterrupt:
    pass

IO.cleanup()
